# Heroku-Buildpack-Sentry

## Purpose

To create a new release on the Sentry.io logging service, to track new deploys of Ember-Client code to all staging apps.

## What happens
*	The buildback installs the **sentry-cli** tool.
*	The buildpack extracts the SHA from the **git** commit that triggered this build, from the `SOURCE_VERSION` environment variable. Note that  `SOURCE_VERSION` is determined by the SHA of the [repo:branch] pushed by Bitbucket.
*	It pulls the authentication token from configuration variable `SENTRY_AUTH_TOKEN` and exports it to make it available for **sentry-cli**. Other command arguments, such as `org` and `project` are hard-coded since they are not confidential information.
*	It creates the new release with `sentry-cli releases new "$SOURCE_VERSION"`.
*	It uploads the Ember-Client sourcemaps to Sentry. Note that it expects the sourcemaps to be found in `public/assets`.

## Public repo

This is a public repository, as Heroku does not support accessing private repositories (requiring authentication) to clone buildpacks.
